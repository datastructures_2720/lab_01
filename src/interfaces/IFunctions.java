package interfaces;

/**
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/]
 *
 */
public interface IFunctions {

	/**
	 * Computes the average of the given array.
	 * @param arr
	 * @return the average
	 */
	public double getAverage(double[] arr);
	
	
}
